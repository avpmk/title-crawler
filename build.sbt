name := "title-crawler"

version := "0.1"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
  guice,
  ws,
  "org.jsoup"       % "jsoup"              % "1.13.1",
  "org.webjars"     % "swagger-ui"         % "3.31.1",
  "io.methvin.play" %% "autoconfig-macros" % "0.3.2"
)

swaggerV3 := true
swaggerDomainNameSpaces := Seq(
  "model"
)

scalacOptions := Seq(
  "-feature",
  "-language:implicitConversions",
  "-Xlint",
  "-Xfatal-warnings"
)

play.sbt.routes.RoutesKeys.routesImport := Seq(
  "model.crawling.Url"
)
