package services

import java.util.concurrent.{TimeoutException => JavaTimeout}

import javax.inject.{Inject, Singleton}
import model.crawling.CrawlingRequest.CrawlingRequest
import model.crawling.ItemResult.CrawlingResponse
import model.crawling.{DocumentData, FailedCrawlingType, FailedItem, TimeoutExhausted, UnexpectedHttpResponse, Url}
import org.jsoup.Jsoup
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CrawlingService @Inject() (
    client: WSClient
) {

  def crawl(
      request: CrawlingRequest
  )(
      implicit
      ec: ExecutionContext
  ): Future[CrawlingResponse] = {
    val futures = for (url <- request) yield {
      crawl(url) map {
        case Right(title) => DocumentData(url, title)
        case Left(error)  => FailedItem(url, error)
      }
    }
    Future sequence futures
  }

  private type Title = String

  def crawl(url: Url)(
      implicit
      ec: ExecutionContext
  ): Future[Either[FailedCrawlingType, Title]] = {
    client
      .url(url.value)
      .get()
      .map { response =>
        if (response.status != 200) {
          Left(UnexpectedHttpResponse)
        } else {
          Right(extractTitle(response))
        }
      }
      .recover {
        case _: JavaTimeout => Left(TimeoutExhausted)
      }
  }

  private def extractTitle(from: WSResponse) = {
    Jsoup.parse(from.body).title()
  }
}
