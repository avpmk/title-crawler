package controllers

import com.google.inject.{Inject, Singleton}
import model.crawling.CrawlingRequest.CrawlingRequest
import model.crawling.Url
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import services.CrawlingService

import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class CrawlingController @Inject() (
    cc: ControllerComponents,
    crawlingService: CrawlingService
) extends AbstractController(cc) {

  def singleCrawl(url: Url) = Action.async {
    crawlingService crawl url map {
      case Right(value) => Ok(Json toJson value)
      case Left(value)  => BadRequest(Json toJson value)
    }
  }

  def batchingCrawl = Action.async(parse.json[CrawlingRequest]) { request =>
    for (xs <- crawlingService.crawl(request.body)) yield {
      Ok(Json toJson xs)
    }
  }
}
