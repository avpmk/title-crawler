package controllers

import javax.inject._
import model.BuildInfo
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc._

@Singleton
class AboutController @Inject() (
    config: Configuration,
    cc: ControllerComponents
) extends AbstractController(cc) {

  private val buildInfo = Json.toJson(config.get[BuildInfo]("build"))

  def about = Action {
    Ok(buildInfo)
  }

  def redirectToSwaggerDoc = Action {
    TemporaryRedirect("docs/swagger-ui/index.html?url=/docs/swagger-ui/swagger.json")
  }
}
