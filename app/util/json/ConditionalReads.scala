package util.json

import play.api.libs.json.Reads
import util.conversion.ConditionalConstructor
import util.conversion.ConditionalConverter.regexConditionalConstructor
import util.conversion.Converter.StringConstructor
import util.json.ReadsOps.toReadsOps

import scala.util.matching.Regex

object ConditionalReads {

  def regexConditionalReads[T: StringConstructor](regex: Regex): Reads[T] = {
    conditionalReads(regexConditionalConstructor[T](regex))
  }

  def conditionalReads[T: Reads, R](c: ConditionalConstructor[T, R]): Reads[R] = {
    implicitly[Reads[T]] validate c.tryConstruct
  }
}
