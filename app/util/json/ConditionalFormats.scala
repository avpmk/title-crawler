package util.json

import play.api.libs.json.Format
import util.conversion.ConditionalConverter
import util.conversion.ConditionalConverter.regexConditionalConverter
import util.conversion.Converter.StringConverter
import util.json.ConditionalReads.conditionalReads
import util.json.WritesHelper.extractingWrites

import scala.util.matching.Regex

object ConditionalFormats {

  def regexConditionalFormat[T: StringConverter](regex: Regex): Format[T] = {
    conditionalFormat(regexConditionalConverter(regex))
  }

  def conditionalFormat[T: Format, R](c: ConditionalConverter[T, R]): Format[R] = {
    Format(conditionalReads(c), extractingWrites(c))
  }
}
