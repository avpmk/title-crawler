package util.json

import play.api.libs.json.{JsString, Writes}
import util.conversion.Extractor

object WritesHelper {

  def createWrites[T](fn: T => String): Writes[T] = Writes(fn andThen JsString)

  def extractingWrites[T, R](
      e: Extractor[T, R]
  )(
      implicit
      writes: Writes[T]
  ): Writes[R] = {
    writes contramap e.extract
  }
}
