package util.json

import play.api.libs.json.{JsError, JsResult, JsSuccess, Reads}
import util.conversion.ConditionalConverter.ErrorMessage

case class ReadsOps[T](self: Reads[T]) extends AnyVal {

  def validate[R](tryConstruct: T => Either[ErrorMessage, R]): Reads[R] = {
    flatMap {
      tryConstruct andThen (_.fold(JsError(_), JsSuccess(_)))
    }
  }

  def flatMap[R](tryConstruct: T => JsResult[R]): Reads[R] = { jsValue =>
    self reads jsValue flatMap tryConstruct
  }
}

object ReadsOps {

  implicit def toReadsOps[T](that: Reads[T]): ReadsOps[T] = ReadsOps(that)
}
