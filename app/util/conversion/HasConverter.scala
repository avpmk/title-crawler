package util.conversion

//todo rename
abstract class HasConverter[T, R](
    implicit
    e: Extractor[T, R]
) extends (T => R) {

  implicit protected val converter: Converter[T, R] = Converter.of(apply _, e)
}
