package util.conversion

import util.conversion.ConditionalConverter.ErrorMessage
import util.conversion.Converter.{create, StringConstructor, StringConverter}

import scala.util.matching.Regex

@FunctionalInterface
trait ConditionalConstructor[T, R] {
  def tryConstruct(from: T): Either[ErrorMessage, R]
}

trait ConditionalConverter[T, R] //
    extends ConditionalConstructor[T, R]
    with Extractor[T, R]

object ConditionalConverter {

  def apply[T, R](
      c: ConditionalConstructor[T, R],
      e: Extractor[T, R]
  ): ConditionalConverter[T, R] = new ConditionalConverter[T, R] {
    override def tryConstruct(from: T): Either[ErrorMessage, R] = c tryConstruct from
    override def extract(from: R): T                            = e extract from
  }

  type ErrorMessage = String

  def regexConditionalConverter[R](regex: Regex)(
      implicit converter: StringConverter[R]
  ): ConditionalConverter[String, R] = ConditionalConverter(
    regexConditionalConstructor(regex),
    converter
  )

  def regexConditionalConstructor[R: StringConstructor](regex: Regex): ConditionalConstructor[String, R] =
    conditionalConstructor[String, ErrorMessage, R](regex.matches)(
      create,
      value => s"Value $value doesn't match regex: $regex"
    )

  private def conditionalConstructor[T, W, R](
      matchCondition: T => Boolean
  )(
      ifMatch: T => R,
      ifNotMatch: T => W
  )(
      value: T
  ): Either[W, R] = {
    if (matchCondition(value))
      Right(ifMatch(value))
    else
      Left(ifNotMatch(value))
  }
}
