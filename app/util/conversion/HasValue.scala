package util.conversion

trait HasValue[T] extends Any {
  def value: T
}

object HasValue {
  implicit def extractor[T, R <: HasValue[T]]: Extractor[T, R] = _.value
}
