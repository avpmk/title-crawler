package util.conversion

trait Constructor[T, R] {
  def construct(from: T): R
}

trait Extractor[T, R] {
  def extract(from: R): T
}

trait Converter[T, R] //
    extends Constructor[T, R]
    with Extractor[T, R]

object Converter {

  def of[T, R](
      c: Constructor[T, R],
      e: Extractor[T, R]
  ): Converter[T, R] = new Converter[T, R] {
    override def construct(from: T): R = c construct from
    override def extract(from: R): T   = e extract from
  }

  type StringConstructor[R] = Constructor[String, R]
  type StringExtractor[R]   = Extractor[String, R]
  type StringConverter[R]   = Converter[String, R]

  //  def create[T, R](from: T)(implicit f: Factory[T, R]): R    = f construct from
  def create[T, R](implicit c: Constructor[T, R]): T => R    = c.construct
  def extract[T, R](from: R)(implicit e: Extractor[T, R]): T = e extract from
}
