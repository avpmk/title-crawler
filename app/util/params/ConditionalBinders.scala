package util.params

import play.api.mvc.QueryStringBindable
import play.api.mvc.QueryStringBindable.bindableString
import util.conversion.ConditionalConverter
import util.conversion.ConditionalConverter.regexConditionalConverter
import util.conversion.Converter.StringConverter

import scala.util.matching.Regex

object ConditionalBinders {

  def regexConditionalBinder[T: StringConverter](regex: Regex): QueryStringBindable[T] = {
    conditionalBinder(regexConditionalConverter[T](regex))
  }

  def conditionalBinder[T, R](c: ConditionalConverter[T, R])(
      implicit
      base: QueryStringBindable[T]
  ): QueryStringBindable[R] = {
    new QueryStringBindable[R] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, R]] = {
        base.bind(key, params) map (_ flatMap c.tryConstruct)
      }
      override def unbind(key: String, value: R): String = base.unbind(key, c.extract(value))
    }
  }
}
