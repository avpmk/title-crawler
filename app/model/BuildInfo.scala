package model

import io.methvin.play.autoconfig.AutoConfig
import play.api.ConfigLoader
import play.api.libs.json.{Json, OWrites}

case class BuildInfo(
    version: String,
    commit: String,
    buildDatetime: String
)

object BuildInfo {

  implicit val writesAppInfo: OWrites[BuildInfo]            = Json.writes[BuildInfo]
  implicit val configLoaderAppInfo: ConfigLoader[BuildInfo] = AutoConfig.loader
}
