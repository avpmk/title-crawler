package model.crawling

import play.api.libs.json.{Json, OWrites}

/** Mapped manually in swagger.yml */
sealed trait ItemResult {
  val url: Url
}

case class DocumentData(
    url: Url,
    title: String
) extends ItemResult

case class FailedItem(
    url: Url,
    error: FailedCrawlingType
) extends ItemResult

object ItemResult {

  type CrawlingResponse = Seq[ItemResult]

  implicit val writesCrawlingResponse: OWrites[ItemResult] = {
    implicit val a = Json.writes[DocumentData]
    implicit val b = Json.writes[FailedItem]
    val _          = (a, b)
    Json.writes[ItemResult]
  }
}
