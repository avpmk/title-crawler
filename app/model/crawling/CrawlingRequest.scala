package model.crawling

object CrawlingRequest {

  /** Mapped manually in swagger.yml */
  type CrawlingRequest = Seq[Url]
}
