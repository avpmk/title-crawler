package model.crawling

import play.api.libs.json.Writes
import util.json.WritesHelper.createWrites

/** Mapped manually in swagger.yml */
sealed trait FailedCrawlingType

object UnexpectedHttpResponse extends FailedCrawlingType
object TimeoutExhausted       extends FailedCrawlingType

object FailedCrawlingType {

  implicit val writesFailedCrawlingType: Writes[FailedCrawlingType] = createWrites {
    case UnexpectedHttpResponse => "UnexpectedHttpResponse"
    case TimeoutExhausted       => "TimeoutExhausted"
  }
}
