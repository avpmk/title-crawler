package model.crawling

import play.api.libs.json.Format
import play.api.mvc.QueryStringBindable
import util.conversion.ConditionalConverter.regexConditionalConverter
import util.conversion.{HasConverter, HasValue}
import util.json.ConditionalFormats.conditionalFormat
import util.params.ConditionalBinders.conditionalBinder

case class Url(
    value: String
) extends AnyVal
    with HasValue[String]

object Url extends HasConverter[String, Url] {

  // https://stackoverflow.com/a/3809435
  private val UrlRegex = """https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)""".r

  private val converterUrl                         = regexConditionalConverter[Url](UrlRegex)
  implicit val formatUrl: Format[Url]              = conditionalFormat(converterUrl)
  implicit val binderUrl: QueryStringBindable[Url] = conditionalBinder(converterUrl)
}
